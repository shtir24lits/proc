﻿#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;

namespace simple_shapes
{
	void Init(struct List &lst);
	void Clear(struct List &lst);
	void In(struct List &lst, ifstream &ifst);
	void Out(struct List &lst, ofstream &ofst);
	void Sort(struct List &lst);
	void OutAphorisms(struct List &lst, ofstream &ofst);
}

using namespace simple_shapes;

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "Incorrect command line! "
			"Waited: command infile outfile" << endl;
		exit(1);
	}

	ifstream ifst(argv[1]);
	ofstream ofst(argv[2]);

	cout << "Start" << endl;
	struct List lst;
	Init(lst);

	In(lst, ifst);
	ofst << "Filled container." << endl;
	Sort(lst);
	Out(lst, ofst);
	OutAphorisms(lst, ofst);
	Clear(lst);
	ofst << "Empty container." << endl;
	Out(lst, ofst);

	cout << "Stop" << endl;
	return 0;
}

